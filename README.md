# brave-bin

Web browser that blocks ads and trackers by default (binary release)

https://brave.com

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/web-browsers/brave-bin.git
```
